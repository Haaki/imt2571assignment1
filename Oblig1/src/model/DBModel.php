<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=localhost;dbname=Assignment1', 'root', '');
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
        $qry = $this->db->query("SELECT * FROM book");
        while ($row = $qry->fetch(PDO::FETCH_ASSOC))
        {
            $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }        
        return $booklist;

    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
        if (is_numeric($id))
        {
            $row = $this->db->query("SELECT * FROM book WHERE id = $id")->fetch(PDO::FETCH_ASSOC);
            if ($row['title'] != NULL)
                $book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {   
        if ($book->title != NULL && $book->author != NULL)
        {
            $qry = $this->db->prepare("INSERT INTO book(title, author, description) VALUES (:title, :author, :description);");
            $qry->execute(array(':title'=>$book->title,':author'=>$book->author, ':description'=>$book->description));
            $book->id = $this->db->query("SELECT * FROM book ORDER BY Id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);   //Why does the book object need to know the id?
        }
        else
        {
            throw new Exception ("Both Title and Author are required fields!");
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        if ($book->title != NULL && $book->author != NULL)
        {
            $qry = $this->db->prepare("UPDATE book SET title = :title, author = :author, description = :description WHERE id = $book->id");
            $qry->execute(array(':title'=>$book->title,':author'=>$book->author, ':description'=>$book->description));
        }
        else
        {
            throw new Exception("Both Title and Author are required fields!");
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        $qry = $this->db->prepare("DELETE FROM book WHERE id = $id");
        $qry->execute();
    }
}

?>